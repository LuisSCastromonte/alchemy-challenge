package com.init.disney.entitys;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name= "peliculasOSerie")

public class peliculaOSerie {
	
	@Id
	@Column(name="titulo")
	private String titulo;
	
	@Column(name="imagenO", nullable=false, length=30)
	private String imagenP;
	
	@Column(name="fechaDeCreacion")
	private Date fechaDeCreacion;
	
	@Column(name="calificacion")
	private Integer calificacion;
	
	
	@ManyToOne
    @JsonBackReference
    private Genero owner;
	
	@ManyToMany(mappedBy="personajes")
    @JsonManagedReference
    private List<Personaje> personajes = new ArrayList<Personaje>();
	
	
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getImagenP() {
		return imagenP;
	}
	public void setImagenP(String imagenP) {
		this.imagenP = imagenP;
	}
	public Date getFechaDeCreacion() {
		return fechaDeCreacion;
	}
	public void setFechaDeCreacion(Date fechaDeCreacion) {
		this.fechaDeCreacion = fechaDeCreacion;
	}
	public Integer getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(Integer calificacion) {
		this.calificacion = calificacion;
	}
	
	
	
	
}
