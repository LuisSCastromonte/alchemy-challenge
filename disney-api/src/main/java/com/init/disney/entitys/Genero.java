package com.init.disney.entitys;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "genero")
public class Genero {
	
	@Id
	@Column(name="nombreGenero")
	private String nombreGenero;
	
	@Column(name="imagenG", nullable=false, length=30)
	private String imagenG;
	
	@OneToMany(mappedBy="owner", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    //@JsonManagedReference
    private List<peliculaOSerie> peliculas = new ArrayList<peliculaOSerie>();
	
		
	public String getNombreGenero() {
		return nombreGenero;
	}
	public void setNombreGenero(String nombreGenero) {
		this.nombreGenero = nombreGenero;
	}
	public String getImagenG() {
		return imagenG;
	}
	public void setImagenG(String imagenG) {
		this.imagenG = imagenG;
	}
	
	
}
