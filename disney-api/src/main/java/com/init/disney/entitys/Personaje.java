package com.init.disney.entitys;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name= "personaje")
public class Personaje {
	
	@Id
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="imagenPersonaje", nullable=false, length=30)
	private String imagenPersonaje;
	
	@Column(name="edad")
	private Integer edad;
	
	@Column(name="peso")
	private Integer peso;
	
	@Column(name="historia", nullable=false, length=100)
	private String historia;
	
	@ManyToMany(mappedBy="peliculasOSeries")
    @JsonManagedReference
    private List<peliculaOSerie> peliculasOSeries = new ArrayList<peliculaOSerie>();
	
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getInmagenPersonaje() {
		return imagenPersonaje;
	}
	public void setInmagenPersonaje(String inmagenPersonaje) {
		this.imagenPersonaje = inmagenPersonaje;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public Integer getPeso() {
		return peso;
	}
	public void setPeso(Integer peso) {
		this.peso = peso;
	}
	public String getHistoria() {
		return historia;
	}
	public void setHistoria(String historia) {
		this.historia = historia;
	}
	
}
