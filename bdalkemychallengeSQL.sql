-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-06-2021 a las 05:15:25
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdalkemychallenge`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `nombreGenero` varchar(200) NOT NULL,
  `imagenG` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`nombreGenero`, `imagenG`) VALUES
('comedia', 'foto comedia'),
('drama', 'foto drama'),
('terror', 'foto terror');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generosdepeliculas`
--

CREATE TABLE `generosdepeliculas` (
  `titulo` varchar(200) NOT NULL,
  `nombreGenero` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `generosdepeliculas`
--

INSERT INTO `generosdepeliculas` (`titulo`, `nombreGenero`) VALUES
('bajo la misma estrella', 'drama'),
('Resident evil', 'terror'),
('y donde estan las rubias?', 'comedia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculaoserie`
--

CREATE TABLE `peliculaoserie` (
  `titulo` varchar(200) NOT NULL,
  `imagenP` varchar(200) NOT NULL,
  `facheDeCreacion` date NOT NULL,
  `calificacion` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `peliculaoserie`
--

INSERT INTO `peliculaoserie` (`titulo`, `imagenP`, `facheDeCreacion`, `calificacion`) VALUES
('bajo la misma estrella', 'foto pareja', '2000-12-03', 3),
('Resident evil', 'foto resident', '2000-12-12', 5),
('y donde estan las rubias?', 'foto rubias', '2001-04-12', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personaje`
--

CREATE TABLE `personaje` (
  `nombre` varchar(100) NOT NULL,
  `imagenPersonaje` varchar(200) NOT NULL,
  `edad` int(11) NOT NULL,
  `peso` int(11) NOT NULL,
  `historia` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `personaje`
--

INSERT INTO `personaje` (`nombre`, `imagenPersonaje`, `edad`, `peso`, `historia`) VALUES
('Ana', 'foto ani', 54, 60, 'ani nacio en canada y es actriz desde los 35'),
('Juan', 'foto juancho', 24, 70, 'Juan nacio en puerto rico y es actor desde los 7'),
('pepe', 'foto pepe', 50, 89, 'pepe nacio en chile y es actor desde los 20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personajesenpelis`
--

CREATE TABLE `personajesenpelis` (
  `titulo` varchar(200) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `personajesenpelis`
--

INSERT INTO `personajesenpelis` (`titulo`, `nombre`) VALUES
('bajo la misma estrella', 'Juan'),
('Resident evil', 'pepe'),
('y donde estan las rubias?', 'Ana');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`nombreGenero`);

--
-- Indices de la tabla `generosdepeliculas`
--
ALTER TABLE `generosdepeliculas`
  ADD KEY `titulo` (`titulo`,`nombreGenero`),
  ADD KEY `nombreGenero` (`nombreGenero`);

--
-- Indices de la tabla `peliculaoserie`
--
ALTER TABLE `peliculaoserie`
  ADD PRIMARY KEY (`titulo`);

--
-- Indices de la tabla `personaje`
--
ALTER TABLE `personaje`
  ADD PRIMARY KEY (`nombre`);

--
-- Indices de la tabla `personajesenpelis`
--
ALTER TABLE `personajesenpelis`
  ADD KEY `titulo` (`titulo`),
  ADD KEY `nombre` (`nombre`),
  ADD KEY `titulo_2` (`titulo`,`nombre`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `generosdepeliculas`
--
ALTER TABLE `generosdepeliculas`
  ADD CONSTRAINT `generosdepeliculas_ibfk_1` FOREIGN KEY (`nombreGenero`) REFERENCES `genero` (`nombreGenero`) ON UPDATE CASCADE,
  ADD CONSTRAINT `generosdepeliculas_ibfk_2` FOREIGN KEY (`titulo`) REFERENCES `peliculaoserie` (`titulo`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `personajesenpelis`
--
ALTER TABLE `personajesenpelis`
  ADD CONSTRAINT `personajesenpelis_ibfk_1` FOREIGN KEY (`nombre`) REFERENCES `personaje` (`nombre`) ON UPDATE CASCADE,
  ADD CONSTRAINT `personajesenpelis_ibfk_2` FOREIGN KEY (`titulo`) REFERENCES `peliculaoserie` (`titulo`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
